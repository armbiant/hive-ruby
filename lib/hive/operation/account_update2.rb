class Hive::Operation::AccountUpdate2 < Hive::Operation
  def_attr account: :string
  def_attr json_metadata: :string
end
